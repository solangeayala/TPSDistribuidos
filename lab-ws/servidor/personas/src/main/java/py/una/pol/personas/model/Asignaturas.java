package py.una.pol.personas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignaturas  implements Serializable {

	
	Long identificador_asignatura;
	String nombre_asignatura;
	
	List <Persona> Alumnos;
	
	public Asignaturas() {
		Alumnos = new ArrayList<Persona>();
	}
	
	public Asignaturas(Long id, String nombre) {
		this.identificador_asignatura = id;
		this.nombre_asignatura = nombre;
		
		Alumnos = new ArrayList<Persona>();
	}
	
	
	public Long getId() {
		return this.identificador_asignatura ;
	}
	
	public void setID(Long ID) {
		this.identificador_asignatura = ID;
	}
	
	public String getNombre() {
		return this.nombre_asignatura;
	}
	
	public void setNombre(String nombre) {
		this.nombre_asignatura = nombre;
	}
	
	public List<Persona> getAlumnos(){
		return this.Alumnos;
	}
	
	public void setAumnos(List<Persona> Alumnos) {
		this.Alumnos = Alumnos;
	}
}
